################################################################
# Global configuration
################################################################
[global]
	checkNewVersion = "false"

	# https://doc.traefik.io/traefik/contributing/data-collection/
	sendAnonymousUsage = "true"

################################################################
# Entrypoints configuration
################################################################
[entryPoints]

	# Encrypted HTTPS
	[entryPoints.web-secure]
		address = ":443"

	# Plain HTTP
	#[entryPoints.web-insecure]
	#	address = ":80"

	# TacticalRMM NATS uses TCP connections
	[entryPoints.tcp-nats]
		address = ":4222"


################################################################
# Certbot automatic TLS
# Enable ACME (Let's Encrypt): automatic SSL.
################################################################
# Note: The name you choose here is the name that needs to be used in
# the router's certResolver. I like to use the provider name for clarity
[certificatesResolvers.cert-cloudflare.acme]
	# CHANGEME: Add your email here. This will be used for the Let's Encrypt account.
	email = "Your-Email@example.com"

	# File or key used for certificates storage.
	storage = "/etc/traefik/acme.json"

	# CA server to use.
	# https://doc.traefik.io/traefik/https/acme/#caserver
	# Required, Default="https://acme-v02.api.letsencrypt.org/directory"
	caServer = "https://acme-v02.api.letsencrypt.org/directory"

	# KeyType used for generating certificate private key.
	# https://doc.traefik.io/traefik/https/acme/#keytype
	# Optional, Default="RSA4096"
	# Allowed values: 'EC256', 'EC384', 'RSA2048', 'RSA4096', 'RSA8192'
	# Elliptical curves are the new shiny...
	#keyType = "EC256"
	# But not all systems are compatible. RSA4096 has almost universal compatibility
	keyType = "RSA4096"

	# Use a DNS-01 ACME challenge rather than HTTP-01 challenge.
	# https://doc.traefik.io/traefik/https/acme/#dnschallenge
	# Note: Mandatory for wildcard certificate generation.
	[certificatesResolvers.cert-cloudflare.acme.dnsChallenge]

		# https://doc.traefik.io/traefik/https/acme/#providers
		provider = "cloudflare"

		# Use your provider's DNS servers to resolve the FQDN authority.
		# CHANGEME: Add your domain DNS provider IPs here.
		resolvers = [ "108.162.194.202:53", "108.162.193.114:53" ]


################################################################
# Traefik logs configuration
################################################################
[log]

	# Log level
	# Default: "ERROR"
	# Accepted values, in order of severity: "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "PANIC"
	level = "INFO"

	# Sets the filepath for the traefik log. If not specified, stdout will be used.
	# Intermediate directories are created if necessary.
	# Default: os.Stdout
	# "os.Stdout" / "Stdout" cannot be specified or it will write to a file called
	# os.Stdout/Stdout.
	filePath = "/var/log/traefik/traefik.log"

	# Format is either "json" or "common".
	# Default: "common"
	format = "json"

################################################################
# Access logs configuration
################################################################
[accessLog]
	# Simply specifying the "accessLog" section enables access logs

	# By default access logs are written to the standard output. To write the logs into a log file,
	# use the filePath option.
	# https://doc.traefik.io/traefik/observability/access-logs/#filepath
	filePath = "/var/log/traefik/access.log"

	# By default, logs are written using the Common Log Format (CLF). To write logs in JSON, use
	# json in the format option. If the given format is unsupported, the default (CLF) is used
	# instead.
	# Format is either "json" or "common".
	format = "common"


################################################################
# API and dashboard configuration
################################################################
[api]
	# Enable API and dashboard
	# Default: true
	dashboard = true
	insecure = false

################################################################
# Ping configuration
################################################################
# Enable ping
#[ping]

	# Name of the related entry point
	# Default: "traefik"
	#entryPoint = "traefik"


################################################################
# Metrics configuration
################################################################
#[metrics]

	# To enable Traefik to export internal metrics to Prometheus
	#[metrics.prometheus]

		# Name of the related entry point
		# Default: "traefik"
		#entryPoint = "traefik"

		# Buckets for latency metrics
		# Default: [0.1, 0.3, 1.2, 5.0]
		#buckets = [0.1, 0.3, 1.2, 5.0]

################################################################
# Provider configuration: File
################################################################
[providers]
	# v2.0 supports templating
	# https://docs.traefik.io/providers/file/
	[providers.file]
	directory = "/etc/traefik/conf.d/"
	# Set the watch option to true to allow Traefik to automatically watch for file changes. It
	# works with both the filename and the directory options.
	watch = "true"


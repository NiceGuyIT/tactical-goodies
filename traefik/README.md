# Traefik for Tactical RMM, standard install

This walks through using Traefik as a reverse proxy for Tactical and other services. Traefik is meant to be installed
on another machine to serve TacticalRMM and other services. TCP proxying is provided for NATS.

While it's possible to install Traefik on any system, this tutorial assumes a GNU Linux based system.

> Note: This configuration was used with a private IP configured in Cloudflare DNS. The domain name will not resolve to
> a public IP or a system on the internet.

## Install

Create the `traefik` user for the systemd service.

```bash
useradd --system --shell /bin/false --user-group --no-create-home traefik
```

Download and install Traefik from the [releases](https://github.com/traefik/traefik/releases) page. This assumes
you're root in the container.

> Important: Download the latest v2 version. Trying to script it to pull the "latest" version may download v1
> (listed first in the releases URL: <https://github.com/traefik/traefik/releases/latest/download/traefik_linux-amd64>.)

```bash
cd /tmp
mkdir traefik
cd traefik/

# This is v2
version="v2.5.3"
curl --remote-name --location "https://github.com/traefik/traefik/releases/download/${version}/traefik_${version}_linux_amd64.tar.gz"

tar -zxf traefik_${version}_linux_amd64.tar.gz
cp traefik /usr/local/bin/traefik
chmod a+x /usr/local/bin/traefik
cd ..
rm -r traefik
```

Install the config.

```bash
mkdir --parents /etc/traefik
cp traefik.toml /etc/traefik
cp traefik-lego.env /etc/traefik
cp -r conf.d /etc/traefik/
chown -R traefik:traefik /etc/traefik/
```

Install the service.

```bash
cp traefik.service /etc/systemd/system/
systemctl daemon-reload

systemctl enable --now traefik.service
```

## Traefik configuration

The naming convention I use is `router-`FQDN`.toml` for the routers. The services for the router are included in the
same file. Hot reloading is enabled and if the configuration is invalid, it will affect only that router/service.
The router and service names are reflected in the Traefik dashboard making it easy to identify the config file
when there are problems.

Rename the `router-*.toml` files to match your architecture and replace the hostnames inside the file.

> Note: Hot reloading is only for the `conf.d/`, not the main `traefik.toml` configuration. If all else fails,
> `systemctl restart traefik.service`.

### /etc/hosts

While it may be possible to configure nginx as the upstream server and accept the hostname info from the downstream
proxy (Traefik), this is not configured out of the box. Therefore, IPs cannot be used. The URL needs to have the FQDN of
the 3 tactical domains.

```toml
[[http.services.tactical-rmm.loadBalancer.servers]]
url = "https://rmm.a8n.tools"
```

Since these domains are on a different IP, DNS cannot be used. Add the upstream server to `/etc/hosts` so that Traefik
can resolve the URLs in the services section.

```text
192.168.1.57    mesh.a8n.tools
192.168.1.57    api.a8n.tools
192.168.1.57    rmm.a8n.tools
```

## Firewall

Since this is meant to fit in your architecture, it's up to you to configure the firewall ports.

## TLS Certificates (Lego Configuration)

`traefik-lego.env` is sourced by systemd to provide the environmental variables to Traefik to acquire Let's Encrypt
certificates. See the [Lego DNS Providers](https://go-acme.github.io/lego/dns/) for which environmental variables need to be set.

`/etc/traefik/traefik-lego.env` is read by `systemd` which is usually owned by `root`. Permissions
matter when using SELinux or systemd specific capabilities. See
[systemd's documentation](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#EnvironmentFile=)
for details. The variables should end with `_FILE` and reference files that contain the actual value.

It's preferable to store tokens in a file so that tokens/secrets don't pollute the environment. This
[response from Lennart Poettering](https://systemd-devel.freedesktop.narkive.com/ibRtoqPS/environment-variable-security#post7)
(systemd developer) highlights why files are preferred.

`/etc/traefik/.*-api-*` files are read by `traefik` and need appropriate permissions.

```bash
chown traefik:traefik /etc/traefik/.*-api-*
chmod go-rwx /etc/traefik/.*-api-*
```

Search for `CHANGEME` in `traefik.toml` and update to match your DNS provider for Lego.

## MeshCentral

MeshCentral requires a configuration change to work with Traefik. This is documented in section 17 of the
[User Guide](https://meshcentral.com/info/docs/MeshCentral2UserGuide.pdf) (PDF).

Specifically, the `CertUrl` needs to point to Traefik to get the cert and `agentConfig` needs to be added.

> Note: Underscores are used as comments in the [advanced configuration example](sample-config-advanced.json).

```json
  "domains": {
    "": {
      "Title": "Tactical RMM",
      "Title2": "Tactical RMM",
      "NewAccounts": false,
      "__CertUrl": "https://mesh.a8n.tools:443/",
      "CertUrl": "https://192.168.1.55:443/",
      "agentConfig": [ "webSocketMaskOverride=1" ],
      "GeoLocation": true,
      "CookieIpCheck": false,
      "mstsc": true
    }
  }
```

## Traefik Dashboard

The Traefik dashboard is also configured by including `[api]` in `traefik.toml`. Basic Authentication is used and a
proper setup will include firewall rules to restrict access to authorized networks/IPs.
